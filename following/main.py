import logging
import requests


username='Yahoo'
userId="19380829"
bearer_token='Bearer AAAAAAAAAAAAAAAAAAAAANRILgAAAAAAnNwIzUejRCOuH5E6I8xnZz4puTs%3D1Zv7ttfk8LF81IUq16cHjhLTvJu4FA33AGWWjCpTnA'



def get_user_followings(userId, username,limit):
    followings = []

    try:
        url = "https://twitter.com/i/api/graphql/taJbMVFxNBcULs8aHwX3cg/Following?variables=%7B%22userId%22%3A%22"+userId+"%22%2C%22count%22%3A"+str(limit)+"%2C%22withHighlightedLabel%22%3Afalse%2C%22withTweetQuoteCount%22%3Afalse%2C%22includePromotedContent%22%3Afalse%2C%22withTweetResult%22%3Afalse%2C%22withUserResults%22%3Afalse%2C%22withNonLegacyCard%22%3Atrue%7D"
        payload = {}
        headers = {
            'authority': 'twitter.com',
            'authorization': bearer_token,
            'x-twitter-client-language': 'tr',
            'x-csrf-token': '931995e3f63304e37fee78dcad62d009a086d318aabedd68d41d9aa30c8d835f00c9d8b6f70ae0de7b25b9d85ef633230209b43087a0b3f1548d10718b3e5a30842649901d023a47b8ada2dfd94b644d',
            'x-twitter-auth-type': 'OAuth2Session',
            'x-twitter-active-user': 'yes',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.72 Safari/537.36',
            'content-type': 'application/json',
            'accept': '*/*',
            'sec-fetch-site': 'same-origin',
            'sec-fetch-mode': 'cors',
            'sec-fetch-dest': 'empty',
            'referer': 'https://twitter.com/'+username+'/following',
            'accept-language': 'en-US,en;q=0.9',
            'cookie': 'personalization_id="v1_8Wsot2mnheEhdgkH6NSZdA=="; guest_id=v1%3A161518089403400498; _sl=1; gt=1368794001550245892; external_referer=padhuUp37zjgzgv1mFWxJ12Ozwit7owX|0|8e8t2xd8A2w%3D; _ga=GA1.2.1400622413.1615180897; _gid=GA1.2.1647128890.1615180897; dnt=1; ads_prefs="HBESAAA="; kdt=GvanZzCKFoRTh9645VD29YnVz3WDySs3zzGDyPM9; remember_checked_on=1; auth_token=8a1256d0b9372522a599d591ee3573b8dbb4fb70; twid=u%3D1011624334987841536; ct0=931995e3f63304e37fee78dcad62d009a086d318aabedd68d41d9aa30c8d835f00c9d8b6f70ae0de7b25b9d85ef633230209b43087a0b3f1548d10718b3e5a30842649901d023a47b8ada2dfd94b644d; at_check=true; mbox=session#dc7d543dc44247ccb660a1fcc84dbdbd#1615185509|PC#dc7d543dc44247ccb660a1fcc84dbdbd.38_0#1678429376; lang=tr'
        }
        response = requests.request("GET", url, headers=headers, data=payload)
        entries = response.json()['data']['user']['following_timeline']['timeline']['instructions'][-1]['entries'][:-2]

        if len(entries) == 0:
            logging.warning("This user is not following anyone")

        elif len(entries) <= limit:
            for i in range(0, len(entries)):
                followings.append(entries[i])

    except Exception as err:
        logging.warning("User followings data could not be scraped for {username} due to {err}".format(username=username,err=err))
    return followings

names=get_user_followings(userId,username,limit=10000)
for i in range(7734):
    print(names[i]['content']['itemContent']['user']['legacy']['name'])


