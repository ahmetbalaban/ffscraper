import logging
import requests
import math

logger = logging.getLogger(__name__)
username='Yahoo'
userId="19380829"
bearer_token='Bearer AAAAAAAAAAAAAAAAAAAAANRILgAAAAAAnNwIzUejRCOuH5E6I8xnZz4puTs%3D1Zv7ttfk8LF81IUq16cHjhLTvJu4FA33AGWWjCpTnA'




def get_user_favs(userId,username,limit):
        try:
                favoritedTweets=[]
                favoritedUsers=[]
                payload = {}
                url = "https://twitter.com/i/api/2/timeline/favorites/" + userId + ".json?include_profile_interstitial_type=1&include_blocking=1&include_blocked_by=1&include_followed_by=1&include_want_retweets=1&include_mute_edge=1&include_can_dm=1&include_can_media_tag=1&skip_status=1&cards_platform=Web-12&include_cards=1&include_ext_alt_text=true&include_quote_count=true&include_reply_count=1&tweet_mode=extended&include_entities=true&include_user_entities=true&include_ext_media_color=true&include_ext_media_availability=true&send_error_codes=true&simple_quoted_tweet=true&sorted_by_time=true&count=" + str(
                        limit) + "&ext=mediaStats%2ChighlightedLabel"
                headers = {
                        'authority': 'twitter.com',
                        'authorization': bearer_token,
                        'x-twitter-client-language': 'tr',
                        'x-csrf-token': '931995e3f63304e37fee78dcad62d009a086d318aabedd68d41d9aa30c8d835f00c9d8b6f70ae0de7b25b9d85ef633230209b43087a0b3f1548d10718b3e5a30842649901d023a47b8ada2dfd94b644d',
                        'x-twitter-auth-type': 'OAuth2Session',
                        'x-twitter-active-user': 'yes',
                        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.72 Safari/537.36',
                        'accept': '*/*',
                        'sec-fetch-site': 'same-origin',
                        'sec-fetch-mode': 'cors',
                        'sec-fetch-dest': 'empty',
                        'referer': 'https://twitter.com/' + username + '/likes',
                        'accept-language': 'en-US,en;q=0.9',
                        'cookie': 'personalization_id="v1_8Wsot2mnheEhdgkH6NSZdA=="; guest_id=v1%3A161518089403400498; _sl=1; _ga=GA1.2.1400622413.1615180897; _gid=GA1.2.1647128890.1615180897; dnt=1; ads_prefs="HBESAAA="; kdt=GvanZzCKFoRTh9645VD29YnVz3WDySs3zzGDyPM9; remember_checked_on=1; auth_token=8a1256d0b9372522a599d591ee3573b8dbb4fb70; twid=u%3D1011624334987841536; ct0=931995e3f63304e37fee78dcad62d009a086d318aabedd68d41d9aa30c8d835f00c9d8b6f70ae0de7b25b9d85ef633230209b43087a0b3f1548d10718b3e5a30842649901d023a47b8ada2dfd94b644d; mbox=PC#dc7d543dc44247ccb660a1fcc84dbdbd.38_0#1678431957|session#f0e2842b0c904025a04d9c93b020b0b0#1615188921; external_referer=8e8t2xd8A2w%3D|0|4abf247TNXg4Rylyqt4v49u2LWyy1%2FaFyfd602NkflM%3D; lang=tr'
                }
                response = requests.request("GET", url, headers=headers, data=payload)
                obj=response.json()
                favoritedTweets.append(obj['globalObjects']['tweets'])
                favoritedUsers.append(obj['globalObjects']['users'])
                instruction = obj['timeline']['instructions'][0]
                entries = instruction['addEntries']['entries']
                for entry in entries:
                        if entry['entryId'] == 'sq-cursor-bottom' or entry['entryId'].startswith('cursor-bottom-'):
                                newCursor = entry['content']['operation']['cursor']['value'].replace("+", "%2B")
                count=0
                while count < 9:
                        response=requests.request("GET",url + "&cursor=" + newCursor,headers=headers,data=payload)
                        obj=response.json()
                        favoritedTweets.append(obj['globalObjects']['tweets'])
                        favoritedUsers.append(obj['globalObjects']['users'])
                        instruction = obj['timeline']['instructions'][0]
                        entries = instruction['addEntries']['entries']
                        for entry in entries:
                                if entry['entryId'] == 'sq-cursor-bottom' or entry['entryId'].startswith(
                                        'cursor-bottom-'):
                                        newCursor = entry['content']['operation']['cursor']['value'].replace("+", "%2B")
                        cursor = newCursor
                        print(cursor)
                        count = count + 1
                return favoritedTweets, favoritedUsers


        except Exception as err:
                logging.warning("User favorites data could not be scraped for {username} due to {err}".format(username=username,err=err))


get_user_favs(userId,username,limit=1000)
print('a')